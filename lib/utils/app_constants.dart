class UrlConstants {
  static const BaseUrl = "https://api.themoviedb.org/3/movie";
  static const TopRatedMovie = '$BaseUrl/top_rated';
  static const ImageBaseUrl = "https://image.tmdb.org/t/p/original/";
  static const NowPlaying = '$BaseUrl/now_playing';
  static const SearchMovie = 'https://api.themoviedb.org/3/search/movie';
}

class AppConstants {
  static const api_key = "7068a48939e88e97665daf97ffd16fc3";
}

class TaskCodes {
  static const FILE_UPLOAD = 1;

  static const GET_ALL_TOP_RATED_MOVIE = 2;

  static const SEARCH_MOVIE = 3;
}
