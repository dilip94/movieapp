import 'package:flutter/material.dart';
import 'package:flutterappdemo/value/color.dart';

class CustomCardView extends StatelessWidget {
  final Widget child;
  final GestureTapCallback onTap;
  final double height;
  final double width;
  final EdgeInsetsGeometry margin;
  final double blurRadius;
  final Color shadowColor;
  final EdgeInsetsGeometry padding;
  final BorderRadiusGeometry borderRadius;
  final Color color;
  final double spreadRadius;

  const CustomCardView(
      {Key key,
      this.onTap,
      this.height,
      this.width,
      this.margin,
      this.blurRadius,
      this.shadowColor,
      this.padding,
      this.child,
      this.borderRadius,
      this.color,
      this.spreadRadius})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
//      decoration: new BoxDecoration(
//          gradient: new LinearGradient(
//              colors: [
//                AppColors.primaryColor,
//                AppColors.primaryLightColor
//              ]
//          ),
//      ),
      decoration:
          BoxDecoration(color: AppColors.fullTransparentColor, boxShadow: [
        BoxShadow(
            color: AppColors.card_elevation_colors,
            blurRadius: blurRadius ?? 4.0,
            offset: Offset(1, 2),
            spreadRadius: spreadRadius ?? 1.0),
      ]),
//    elevation: 4,
//      shape: RoundedRectangleBorder(
//        borderRadius: borderRadius??BorderRadius.all(Radius.circular(4)),
//      ),
      child: InkWell(
        onTap: onTap,
        child: ClipRRect(
          child: Container(
            child: child,
            width: width != null ? width : double.maxFinite,
            height: height,
            padding: padding ?? EdgeInsets.all(0),
            color: Colors.white,
          ),
          borderRadius: borderRadius ?? BorderRadius.all(Radius.circular(4)),
        ),
      ),
      margin: margin != null ? margin : EdgeInsets.all(10),
    );
  }
}
