import 'package:flutter/material.dart';
import 'package:flutterappdemo/models/top_rated/now_playing.dart';
import 'package:flutterappdemo/ui_screen/base/base_stateless_screen.dart';
import 'package:flutterappdemo/ui_screen/card/card.dart';
import 'package:flutterappdemo/ui_screen/custom_view/cache_images/cached_image_widget.dart';
import 'package:flutterappdemo/ui_screen/custom_view/sub_title_text.dart';
import 'package:flutterappdemo/ui_screen/custom_view/title_text.dart';
import 'package:flutterappdemo/ui_screen/home/new_movie_detail_screen.dart';
import 'package:flutterappdemo/utils/app_constants.dart';
import 'package:flutterappdemo/value/color.dart';

class NowPlayingMovieRow extends BaseStateless {
  final Results result;

  NowPlayingMovieRow({Key key, this.result});

  @override
  Widget build(BuildContext context) {
    return CustomCardView(
      margin: EdgeInsets.only(left: 20, right: 20, top: 16, bottom: 4),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          onTap: () {
            changeScreen(
                NewMovieDetailScreen(
                  result: result,
                ),
                context);
          },
          child: Row(
            children: <Widget>[
              CachedNetworkImage(
                imageUrl: '${UrlConstants.ImageBaseUrl}${result.poster_path}',
                height: 40,
                width: 40,
                fit: BoxFit.fill,
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(left: 11),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TitleText(
                        margin: EdgeInsets.only(top: 8),
                        text: result.title,
                        color: AppColors.titleTextColor,
                        textSize: 16.0,
                        overflow: TextOverflow.visible,
                        alignment: Alignment.centerLeft,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: SubTitle(
                          overflow: TextOverflow.visible,
                          textSize: 12,
                          text: result.original_title,
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
