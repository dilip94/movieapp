import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterappdemo/network/ApiTask.dart';
import 'package:flutterappdemo/ui_screen/custom_view/sub_title_text.dart';
import 'package:flutterappdemo/ui_screen/custom_view/title_text.dart';
import 'package:flutterappdemo/utils/app_constants.dart';
import 'package:flutterappdemo/value/color.dart';

abstract class BaseState<T extends StatefulWidget> extends State<T>
    implements TaskCallBack {
  ApiTask _apiTask;
  static const String RESULT_OK = 'RESULT_OK';
  bool alive = false;
  bool progress = false;

//  void init(BuildContext context) {
//    Preferences.init();
//  }

  @override
  void initState() {
    super.initState();
//    Preferences.init();
    _apiTask = ApiTask(callBack: this);
  }

  @override
  void dispose() {
    super.dispose();
    alive = false;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    alive = true;
  }

  void executeTask(int taskCode, HttpParamObject httpParamObject) {
    isInternetConnect().then((isConnected) {
      if (isConnected) {
        _apiTask.executeTask(taskCode, httpParamObject);
      }
    });
  }

  @override
  onPreExecute(int taskCode) {
    switch (taskCode) {
      case TaskCodes.FILE_UPLOAD:
        return;
    }
    showProgress();
  }

  @override
  onPostExecute(
      int taskCode, dynamic response, HttpParamObject httpParamObject) {
    hideProgress();
    if (response == null) {
      return;
    }
    switch (taskCode) {
      case TaskCodes.FILE_UPLOAD:
        break;
    }
    if (response != null) {
//      checkSession(response);
    }
  }

  @override
  onError(e) {
    debugPrint("onError: $e");
    hideProgress();
  }

  void showProgress() {
    progress = true;
    if (alive) {
      setState(() {});
    }
  }

  void hideProgress() {
    progress = false;
    if (alive) {
      setState(() {});
    }
  }

  Future<void> showToast(String message) async {
    var flushbar = Flushbar(
      message: message,
      duration: Duration(seconds: 3),
      flushbarPosition: FlushbarPosition.TOP,
    );
    await flushbar.show(context);
  }

  void finishScreen() {
    Navigator.pop(context);
  }

  void changeScreen(Widget screen, {bool animation, String screenName}) {
    var router = MaterialPageRoute(
      builder: (context) {
        return screen;
      },
      settings: RouteSettings(name: screenName ?? screen.toString()),
    );
//    if (animation == true) {
//      var router = SlideTopRoute(page: screen,);
//      Navigator.push(context, router);
//      return;
//    }
  }

  void finishAndChangeScreen(screen, {bool animation, String screenName}) {
    var materialPageRoute = MaterialPageRoute(
      builder: (context) {
        return screen;
      },
      settings: RouteSettings(name: screenName ?? screen.toString()),
    );
  }

  void changeScreenAndPop(Widget screen, {bool animation, String screenName}) {
    Navigator.pop(context);
    changeScreen(screen, animation: animation, screenName: screenName);
  }

  Future<T> changeScreenForResult<T>(Widget screen,
      {bool animation, String screenName}) async {
    var route = MaterialPageRoute<T>(
      builder: (BuildContext context) {
        return screen;
      },
      settings: RouteSettings(name: screenName ?? screen.toString()),
    );
    return await Navigator.of(context).push(route);
  }

  setResult<T>(T value, BuildContext context) {
    return Navigator.of(context).pop(value);
  }

  Future<bool> isInternetConnect() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    showToast('no_internet');
    return false;
  }

  horizontalLine({Color color}) {
    return Container(
      height: 1.0,
      color: color != null ? color : AppColors.lineColor,
    );
  }

  void showAlertDialog(
      {String title,
      String content,
      VoidCallback onPressedYes,
      VoidCallback onPressedNo,
      String positiveBtnText,
      String negativeBtnText}) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: TitleText(
              text: title,
            ),
            content: SubTitle(
              text: content,
            ),
            actions: <Widget>[
              FlatButton(
                child:
                    new Text(negativeBtnText != null ? negativeBtnText : 'no'),
                onPressed: () {
                  finishScreen();
                  onPressedNo();
                },
              ),
              FlatButton(
                child:
                    new Text(positiveBtnText != null ? positiveBtnText : 'yes'),
                onPressed: () {
                  finishScreen();
                  onPressedYes();
                },
              ),
            ],
          );
        });
  }

  changeFocus(FocusNode focus) {
    return FocusScope.of(context).requestFocus(focus);
  }

  hideKeyboard() {
    return FocusScope.of(context).requestFocus(FocusNode());
  }

  finishAndStartHomeScreen() {
    return Navigator.of(context)
        .pushNamedAndRemoveUntil('/ui_screen.home', (Route<dynamic> route) => false);
  }

  finishAndStartLoginScreen() {
    return Navigator.of(context)
        .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
  }
}
