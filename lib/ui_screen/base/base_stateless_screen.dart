import 'dart:async';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutterappdemo/value/color.dart';

abstract class BaseStateless extends StatelessWidget {
  static const String RESULT_OK = 'RESULT_OK';

  void showToast(String message, context) {
    Flushbar(
      title: message,
      duration: Duration(seconds: 3),
      flushbarPosition: FlushbarPosition.TOP,
    )..show(context);
  }

  void finishScreen(context) {
    Navigator.pop(context);
  }

  void changeScreen(Widget screen, context, {String screenName}) {
    var router = MaterialPageRoute(
      builder: (context) {
        return screen;
      },
      settings: RouteSettings(name: screenName ?? screen.toString()),
    );
    Navigator.push(context, router);
  }

  void finishAndChangeScreen(screen, context, {String screenName}) {
    var materialPageRoute = MaterialPageRoute(
      builder: (context) {
        return screen;
      },
      settings: RouteSettings(name: screenName ?? screen.toString()),
    );
    Navigator.pushReplacement(context, materialPageRoute);
  }

  void changeScreenAndPop(Widget screen, context) {
    Navigator.pop(context);
    changeScreen(screen, context);
  }

  Future<T> changeScreenForResult<T>(Widget screen, context,
      {String screenName}) async {
    return await Navigator.of(context).push(new MaterialPageRoute<T>(
      builder: (BuildContext context) {
        return screen;
      },
      settings: RouteSettings(name: screenName ?? screen.toString()),
    ));
  }

  setResult<T>(T value, BuildContext context) {
    return Navigator.of(context).pop(value);
  }

  horizontalLine({Color color}) {
    return Container(
      height: 1.0,
      color: color != null ? color : AppColors.lineColor,
    );
  }

  hideKeyboard(context) {
    return FocusScope.of(context).requestFocus(FocusNode());
  }

  finishAndStartHomeScreen(context) {
    return Navigator.of(context)
        .pushNamedAndRemoveUntil('/ui_screen.home', (Route<dynamic> route) => false);
  }

  finishAndStartLoginScreen(context) {
    return Navigator.of(context)
        .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
  }
}
