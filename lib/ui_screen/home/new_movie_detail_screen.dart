import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutterappdemo/models/top_rated/now_playing.dart';
import 'package:flutterappdemo/ui_screen/custom_view/cache_images/cached_image_widget.dart';
import 'package:flutterappdemo/ui_screen/custom_view/sub_title_text.dart';
import 'package:flutterappdemo/ui_screen/custom_view/title_text.dart';
import 'package:flutterappdemo/utils/app_constants.dart';
import 'package:flutterappdemo/value/color.dart';

class NewMovieDetailScreen extends StatelessWidget {
  final Results result;

  const NewMovieDetailScreen({Key key, this.result}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var ratingStar = 0;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: TitleText(
            text: "New Movie Detail page",
          ),
        ),
        body: Container(
          padding: EdgeInsets.all(8),
          child: ListView(
            children: <Widget>[
              CachedNetworkImage(
                imageUrl: '${UrlConstants.ImageBaseUrl}${result.poster_path}',
                height: 320,
                width: 320,
                fit: BoxFit.fill,
              ),
              TitleText(
                margin: EdgeInsets.only(top: 8),
                text: result.title,
                color: AppColors.titleTextColor,
                textSize: 16.0,
                overflow: TextOverflow.visible,
                alignment: Alignment.centerLeft,
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: SubTitle(
                  overflow: TextOverflow.visible,
                  textSize: 12,
                  text: result.overview,
                ),
              ),
              RatingBar(
                initialRating: result.vote_average ?? 0,
                minRating: 10,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 10,
                ratingWidget: RatingWidget(
                  full: Icon(
                    Icons.star,
                    size: 16,
                    color: Colors.amber,
                  ),
                  half: Icon(
                    Icons.star_half,
                    size: 16,
                    color: Colors.amber,
                  ),
                  empty: Icon(
                    Icons.star,
                    size: 16,
                    color: Colors.grey,
                  ),
                ),
                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
//                itemBuilder: (context, _) => Icon(
//                  Icons.star,
//                  color: Colors.amber,
//                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
