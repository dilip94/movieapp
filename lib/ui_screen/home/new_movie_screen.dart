import 'package:flutter/material.dart';
import 'package:flutterappdemo/models/top_rated/now_playing.dart';
import 'package:flutterappdemo/network/ApiTask.dart';
import 'package:flutterappdemo/network/api_request_generator.dart';
import 'package:flutterappdemo/ui_screen/base/base_stateful_screen.dart';
import 'package:flutterappdemo/ui_screen/custom_view/title_text.dart';
import 'package:flutterappdemo/ui_screen/drawer/drawer.dart';
import 'package:flutterappdemo/ui_screen/row/new_movie_row.dart';
import 'package:flutterappdemo/utils/app_constants.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends BaseState<HomeScreen> {
  List<Results> newMovieList = [];
  Icon actionIcon = new Icon(Icons.search);
  Widget appBarTitle = new TitleText(
    text: "New Moview",
    color: Colors.white,
    textSize: 16,
  );

  @override
  void initState() {
    super.initState();
    getNewMovie();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(centerTitle: true, title: appBarTitle,
//              leading: Icon(Icons.local_movies),
              actions: <Widget>[
                new IconButton(
                  icon: actionIcon,
                  onPressed: () {
                    setState(() {
                      if (this.actionIcon.icon == Icons.close) {
                        print("onClose");
                        getNewMovie();
                      }
                      if (this.actionIcon.icon == Icons.search) {
                        this.actionIcon = new Icon(Icons.close);
                        this.appBarTitle = new TextField(
                          onChanged: (query) {
                            print("onChanged");
                            if (query.isEmpty) {
                              getNewMovie();
                            } else {
                              searchMovies(query);
                            }
                          },
                          style: new TextStyle(
                            color: Colors.white,
                          ),
                          decoration: new InputDecoration(
                              prefixIcon:
                                  new Icon(Icons.search, color: Colors.white),
                              hintText: "Search...",
                              hintStyle: new TextStyle(color: Colors.white)),
                        );
                      } else {
                        this.actionIcon = new Icon(Icons.search);
                        this.appBarTitle = new Text("New Moview");
                      }
                    });
                  },
                ),
              ]),
          drawer: AppDrawer(),
          body: ListView.builder(
            itemCount: newMovieList.length,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              Results topRatedMovieModel = newMovieList[index];
              return NowPlayingMovieRow(
                result: topRatedMovieModel,
              );
            },
          )),
    );
  }

  getNewMovie() {
    HttpParamObject httpParamObject = ApiRequestGenerator.getAllTopRatedMovie();
    executeTask(TaskCodes.GET_ALL_TOP_RATED_MOVIE, httpParamObject);
  }

  searchMovies(String query) {
    HttpParamObject httpParamObject = ApiRequestGenerator.searchMovie(query);
    executeTask(TaskCodes.SEARCH_MOVIE, httpParamObject);
  }

  @override
  onPostExecute(
      int taskCode, dynamic response, HttpParamObject httpParamObject) {
    super.onPostExecute(taskCode, response, httpParamObject);
    if (response == null) {
      showToast('server_error');
      return;
    }
    switch (taskCode) {
      case TaskCodes.GET_ALL_TOP_RATED_MOVIE:
        NowPlayingMovieResponse topRatedMovieResponse = response;
        if (topRatedMovieResponse.results != null) {
          newMovieList.clear();
          newMovieList.addAll(topRatedMovieResponse.results);
        }
        setState(() {});

        break;
      case TaskCodes.SEARCH_MOVIE:
        NowPlayingMovieResponse topRatedMovieResponse = response;
        if (topRatedMovieResponse.results != null) {
          newMovieList.clear();
          newMovieList.addAll(topRatedMovieResponse.results);
        }
        setState(() {});
    }
  }
}
