import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterappdemo/ui_screen/custom_view/cache_images/cached_image_widget.dart';
import 'package:flutterappdemo/value/color.dart';
import 'package:validators/validators.dart';

class CacheNetworkImage extends StatelessWidget {
  final String imageUrl;
  final double width;
  final double height;
  final BoxFit fit;

  const CacheNetworkImage(
      {Key key, this.imageUrl, this.width, this.height, this.fit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      alignment: Alignment.center,
      child: imageUrl == null
          ? Container(
              alignment: Alignment.topCenter,
              height: height,
              width: width,
              color: AppColors.hintColor,
            )
          : isURL(imageUrl) && imageUrl.endsWith('.svg')
              ? SvgPicture.network(
                  imageUrl,
                  height: height,
                  width: width,
                )
              : isURL(imageUrl)
                  ? CachedNetworkImage(
                      height: height,
                      width: width,
                      imageUrl: imageUrl,
                      imageBuilder: (context, imageProvider) => Container(
                        height: height,
                        width: width,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: imageProvider,
                            fit: fit ?? null,
//                            colorFilter: ColorFilter.mode(
//                                Colors.red, BlendMode.colorBurn)
                          ),
                        ),
                      ),
                      placeholder: (context, url) => Container(
                          height: 16,
                          width: 16,
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(
                            strokeWidth: 2,
                          )),
//                 `         errorWidget: (context, url, error) =>
//                              Icon(Icons.error),
                    )
                  : !isURL(imageUrl) && imageUrl.endsWith('.svg')
                      ? SvgPicture.asset(
                          imageUrl,
                          height: height,
                          width: width,
                        )
                      : !imageUrl.endsWith('.svg')
                          ? Image.asset(
                              imageUrl,
                              height: height,
                              width: width,
                            )
                          : Container(
                              alignment: Alignment.center,
                              height: height,
                              width: width,
                              color: AppColors.hintColor,
                            ),
    );
  }
}
