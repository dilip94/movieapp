import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutterappdemo/value/color.dart';

import 'base/base_stateful_screen.dart';
import 'home/new_movie_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends BaseState<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 3), () async {
      homeScreen(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.splashColor,
    );
  }

  void homeScreen(BuildContext context) {
    var enterPhoneNoRoute =
        new MaterialPageRoute(builder: (BuildContext context) {
      return HomeScreen();
    });
    Navigator.of(context).pushReplacement(enterPhoneNoRoute);
  }
}
