import 'dart:ui';

class AppColors {
  static final lineColor = Color(0xff22b1c3);
  static final titleTextColor = Color(0xff2a353e);
  static final subTitleTextColor = Color(0xff2a353e);
  static final hintColor = Color(0xff949a9e);
  static final fullTransparentColor = Color(0x00000000);
  static const card_elevation_colors = Color(0x194a4a4a);

  static var splashColor = Color(0xff22b1c3);
}
