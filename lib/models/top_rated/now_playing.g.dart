// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'now_playing.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NowPlayingMovieResponse _$NowPlayingMovieResponseFromJson(
    Map<String, dynamic> json) {
  return NowPlayingMovieResponse()
    ..results = (json['results'] as List)
        ?.map((e) =>
            e == null ? null : Results.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..page = json['page'] as int
    ..total_results = json['total_results'] as int
    ..dates = json['dates'] == null
        ? null
        : Dates.fromJson(json['dates'] as Map<String, dynamic>)
    ..total_pages = json['total_pages'] as int;
}

Map<String, dynamic> _$NowPlayingMovieResponseToJson(
        NowPlayingMovieResponse instance) =>
    <String, dynamic>{
      'results': instance.results,
      'page': instance.page,
      'total_results': instance.total_results,
      'dates': instance.dates,
      'total_pages': instance.total_pages,
    };

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results()
    ..popularity = (json['popularity'] as num)?.toDouble()
    ..vote_count = json['vote_count'] as int
    ..video = json['video'] as bool
    ..poster_path = json['poster_path'] as String
    ..id = json['id'] as int
    ..adult = json['adult'] as bool
    ..backdrop_path = json['backdrop_path'] as String
    ..original_language = json['original_language'] as String
    ..original_title = json['original_title'] as String
    ..genre_ids = (json['genre_ids'] as List)?.map((e) => e as int)?.toList()
    ..title = json['title'] as String
    ..vote_average = (json['vote_average'] as num)?.toDouble()
    ..overview = json['overview'] as String
    ..release_date = json['release_date'] as String;
}

Map<String, dynamic> _$ResultsToJson(Results instance) => <String, dynamic>{
      'popularity': instance.popularity,
      'vote_count': instance.vote_count,
      'video': instance.video,
      'poster_path': instance.poster_path,
      'id': instance.id,
      'adult': instance.adult,
      'backdrop_path': instance.backdrop_path,
      'original_language': instance.original_language,
      'original_title': instance.original_title,
      'genre_ids': instance.genre_ids,
      'title': instance.title,
      'vote_average': instance.vote_average,
      'overview': instance.overview,
      'release_date': instance.release_date,
    };

Dates _$DatesFromJson(Map<String, dynamic> json) {
  return Dates()
    ..maximum = json['maximum'] as String
    ..minimum = json['minimum'] as String;
}

Map<String, dynamic> _$DatesToJson(Dates instance) => <String, dynamic>{
      'maximum': instance.maximum,
      'minimum': instance.minimum,
    };
