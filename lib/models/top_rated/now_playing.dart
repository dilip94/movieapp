import 'package:json_annotation/json_annotation.dart';

part 'now_playing.g.dart';

@JsonSerializable()
class NowPlayingMovieResponse {
  List<Results> results;
  int page;
  int total_results;
  Dates dates;
  int total_pages;

  NowPlayingMovieResponse();

  factory NowPlayingMovieResponse.fromJson(Map<String, dynamic> json) =>
      _$NowPlayingMovieResponseFromJson(json);

  Map<String, dynamic> toJson() => _$NowPlayingMovieResponseToJson(this);

  NowPlayingMovieResponse parseJson(Map<String, dynamic> json) {
    return NowPlayingMovieResponse.fromJson(json);
  }
}

@JsonSerializable()
class Results {
  double popularity;
  int vote_count;
  bool video;
  String poster_path;
  int id;
  bool adult;
  String backdrop_path;
  String original_language;
  String original_title;
  List<int> genre_ids;
  String title;
  double vote_average;
  String overview;
  String release_date;

  Results();

  factory Results.fromJson(Map<String, dynamic> json) =>
      _$ResultsFromJson(json);

  Map<String, dynamic> toJson() => _$ResultsToJson(this);
}

@JsonSerializable()
class Dates {
  String maximum;
  String minimum;

  Dates();

  factory Dates.fromJson(Map<String, dynamic> json) => _$DatesFromJson(json);

  Map<String, dynamic> toJson() => _$DatesToJson(this);
}
