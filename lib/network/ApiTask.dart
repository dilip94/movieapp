import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ApiTask {
  final TaskCallBack callBack;

  const ApiTask({this.callBack});

  Future<dynamic> executeTask(int taskCode, HttpParamObject httpParamObject) {
    callBack?.onPreExecute(taskCode);
    var response = getData(httpParamObject);
    response.then((object) {
      callBack?.onPostExecute(taskCode, object, httpParamObject);
    }).catchError((onError) {
      callBack?.onError(onError);
    });
    return response;
  }

  Future<dynamic> getData(HttpParamObject httpParamObject) async {
    var response;
    httpParamObject.addHeader("Content-Type", httpParamObject._contentType);
    switch (httpParamObject._method) {
      case Method.GET:
        response = await http.get(httpParamObject.getUrlWithParams(),
            headers: httpParamObject._headers);
        break;
      case Method.POST:
        if (httpParamObject._contentType == 'application/json') {
          var jsonData = getJsonData(httpParamObject.json);
          response = await http.post(httpParamObject._url,
              headers: httpParamObject._headers, body: jsonEncode(jsonData));
        } else if (httpParamObject._contentType == 'multipart/form-data') {
          FormData formData = new FormData.fromMap(httpParamObject._params);
          response = await Dio().post(httpParamObject._url, data: formData);
          var decode = json.decode(response.toString());
          return parseJson(decode, httpParamObject);
        } else {
          response = await http.post(httpParamObject._url,
              headers: httpParamObject._headers, body: httpParamObject._params);
        }
        break;
      case Method.PUT:
        if (httpParamObject._contentType == 'application/json') {
          var jsonData = getJsonData(httpParamObject.json);
          response = await http.put(httpParamObject.getUrlWithParams(),
              headers: httpParamObject._headers, body: jsonEncode(jsonData));
        } else {
          response = await http.put(httpParamObject.getUrlWithParams(),
              headers: httpParamObject._headers, body: httpParamObject._params);
        }
        break;
      case Method.DELETE:
        response = await http.delete(
          httpParamObject.getUrlWithParams(),
          headers: httpParamObject._headers,
        );
        break;
      case Method.PATCH:
        if (httpParamObject._contentType == 'application/json') {
          response = await http.patch(httpParamObject.getUrlWithParams(),
              headers: httpParamObject._headers,
              body: jsonEncode(getJsonData(httpParamObject.json)));
        } else {
          response = await http.patch(httpParamObject.getUrlWithParams(),
              headers: httpParamObject._headers, body: httpParamObject._params);
        }
        break;
    }
    if (response.body.toString().isEmpty) {
      throw Exception('Empty response!');
    }
    var decode = json.decode(response.body.toString());
    try {
      if (decode["code"] == null) {
        decode["error"] = true;
        decode["code"] = "${decode["status"]}";
        decode["message"] = decode["message"];
      }
    } catch (e) {}

    return parseJson(decode, httpParamObject);
  }

  Object parseJson(decode, HttpParamObject httpParamObject) {
    debugPrint("RequestUrl: ${httpParamObject.toString()}");
    debugPrint("parseJson:$decode");
    try {
      return httpParamObject.classType.parseJson(decode);
    } catch (e) {
      debugPrint("parseError:$e");
    }
    return decode;
  }

  static Map<String, dynamic> getJsonData(Map<String, dynamic> json) {
    Map<String, dynamic> newJson = Map();
    json.forEach((key, value) {
      try {
        Map<String, dynamic> innerJson = value.toJson();
        newJson[key] = getJsonData(innerJson);
      } catch (e) {
        try {
          var values = [];
          for (var arrayValue in value) {
            try {
              Map<String, dynamic> innerJson = arrayValue.toJson();
              values.add(getJsonData(innerJson));
            } catch (e) {
              values.add(arrayValue);
            }
          }
          newJson[key] = values;
        } catch (e) {
          newJson[key] = value;
        }
      }
    });
    return newJson;
  }
}

class HttpParamObject {
  String _url;
  String _contentType = "application/json";
  Method _method = Method.GET;
  Map<String, dynamic> _json;
  Map<String, String> _headers = {};
  Map<String, dynamic> _params = {};
  dynamic classType;
  dynamic object;

//  HttpParamObject() {
//    if (Preferences.isLoggedIn()) {
//      addHeader("roleType", Preferences.getString(PrefKeys.ROLE_TYPE, '2'));
//      addHeader("authToken", Preferences.getString(PrefKeys.TOKEN, ''));
//    }
//  }

  String get url => _url;

  set url(String value) {
    _url = value;
  }

  Method get method => _method;

  set method(Method value) {
    _method = value;
  }

  Map<String, dynamic> get json => _json;

  Map<String, String> get headers => _headers;

  set json(Map<String, dynamic> value) {
    _json = value;
  }

  void addHeader(String key, dynamic value) {
    _headers[key] = value;
  }

  void addParam(String key, dynamic value) {
    _params[key] = value;
  }

  String getUrlWithParams() {
    var stringBuffer = StringBuffer(_url);
    if (_params.length > 0) {
      stringBuffer.write('?');
      _params.forEach((String key, dynamic value) {
        stringBuffer.write('$key=$value&');
      });
    }
    return stringBuffer.toString();
  }

  void setJsonContentType() {
    _contentType = "application/json";
  }

  void setMultipartContentType() {
    _contentType = "multipart/form-data";
  }

  @override
  String toString() {
    return 'HttpParamObject{_url: $_url, _contentType: $_contentType, _method: $_method, _json: $_json, _headers: $_headers, _params: $_params, classType: $classType}';
  }

  Map<String, String> get params => _params;
}

enum Method {
  GET,
  POST,
  PUT,
  DELETE,
  PATCH,
}

abstract class TaskCallBack {
  void onPreExecute(int taskCode);

  void onPostExecute(
      int taskCode, dynamic response, HttpParamObject httpParamObject);

  void onError(e);
}
