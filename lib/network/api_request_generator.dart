import 'dart:core';

import 'package:flutterappdemo/models/top_rated/now_playing.dart';
import 'package:flutterappdemo/network/ApiTask.dart';
import 'package:flutterappdemo/utils/app_constants.dart';

class ApiRequestGenerator {
  static HttpParamObject getAllTopRatedMovie() {
    HttpParamObject httpParamObject = HttpParamObject();
    httpParamObject.url = UrlConstants.NowPlaying;
    httpParamObject.method = Method.GET;
    httpParamObject.setJsonContentType();
    httpParamObject.addParam('api_key', AppConstants.api_key);
    httpParamObject.classType = NowPlayingMovieResponse();
    return httpParamObject;
  }

  static HttpParamObject searchMovie(String query) {
    HttpParamObject httpParamObject = HttpParamObject();
    httpParamObject.url = UrlConstants.SearchMovie;
    httpParamObject.method = Method.GET;
    httpParamObject.setJsonContentType();
    httpParamObject.addParam('api_key', AppConstants.api_key);
    httpParamObject.addParam('query', query);
    httpParamObject.classType = NowPlayingMovieResponse();
    return httpParamObject;
  }
}
